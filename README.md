# Aiven Terraform Provider combined with Gitlab CI

This repo demonstrate how to use Gitlab CI to apply Terraform configuration files and create resources in your Aiven account.

## Setting your Aiven auth token

Have you Aiven auth token ready a go to Settings -> CI -> Variables and add you auth token with the key `TF_VAR_aiven_api_token`. 

![](gitlab-secret.png)

## Push to test

Push a new terraform configuration file or update the existing one and you should see the GH Action start running and services appearing on your Aiven Console.
For instance, you can try to add a new Kafka Topic in `kafka.tf` like : 

```
resource "aiven_kafka_topic" "topic-a-second-topic" {
  project      = "dev-sandbox"
  service_name = aiven_kafka.sblanc-kafka-terraform-test.service_name
  topic_name   = "a-second-topic"
  partitions   = 3
  replication  = 2
}

```